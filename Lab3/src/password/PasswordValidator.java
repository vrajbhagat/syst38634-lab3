package password;

public class PasswordValidator {
	
	public static int count = 0;
	
	public static void main(String[] args) {
		
	}
	
	public static boolean checkPasswordLength ( String password ) throws StringIndexOutOfBoundsException {
		
		if ( password.length() >= 8 ) {
			return true;
		}
		else {
			throw new StringIndexOutOfBoundsException("Invalid password length " + password);
		}
		
//		After refactoring=>
//		return password.length() >= 8;
		
//      Before refactoring=>		
//		if ( password.length() >= 8 ) {
//			return true;
//		}
//		else {
//			return false;
//		}
	} 
	
	 public static boolean checkPasswordDigits(String password) throws StringIndexOutOfBoundsException {

//       After Refactoring=>		 
		 if (password.length() >= 8 ) {
		        for(int i = 0; i < password.length(); i++) {
		            char pc = password.charAt(i);
		            if (!Character.isLetterOrDigit(pc)) {
		            	 return false;
		            }
		            else {
		                if (Character.isDigit(pc)) {
		                    count++;
		                }	             
		            } 
		        }
		    }
		 else {
			 throw new StringIndexOutOfBoundsException("Invalid password length " + password);
		 }
		 if(count < 2) {
			 return false;
		 }
		return true;
	 }
	 
}

