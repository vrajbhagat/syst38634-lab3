package password;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

// Four Tests for testCheckPasswordLength() => 

	// A Regular Test that fails
//	@Test
//	public void testCheckPasswordLength() {
//		boolean password = PasswordValidator.checkPasswordLength( "passwor" );
//		System.out.println(password);
//		assertTrue("Invalid password length " , password );
//	}
	
	// A Regular Test that Passes = Happy Test
	@Test
	public void testCheckPasswordLength() {
		boolean password = PasswordValidator.checkPasswordLength( "password" );
		System.out.println(password);
		assertTrue("Invalid password length " , password );
	}
	
	// Exception Test = Sad Test
	@Test ( expected = StringIndexOutOfBoundsException.class)
	public void testCheckPasswordLengthException() {
		boolean password = PasswordValidator.checkPasswordLength( "p" );
		System.out.println(password);
//		assertTrue("Invalid password length ", password );
	}
	
	// Boundary In Test = use length greater than 8
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean password = PasswordValidator.checkPasswordLength( "password123" );
		System.out.println(password);
		assertTrue("Invalid password length " , password );
	}
	
	// Boundary Out Test = use length smaller than 8
	@Test  ( expected = StringIndexOutOfBoundsException.class)
	public void testCheckPasswordLengthBoundaryOut() {
		boolean password = PasswordValidator.checkPasswordLength( "p12" );
		System.out.println(password);
//		assertTrue("Invalid password length " , password );
	}
	
//	Four Test for testCheckPasswordDigits() =>
	
	// A Regular Test that Passes = Happy Test
	@Test
	public void testCheckPasswordDigits() {
		boolean password = PasswordValidator.checkPasswordDigits("password12");
		System.out.println(password);
		assertTrue("Password does not have enough numbers ", password);
	}
	
	// Exception Test = Sad Test
	@Test ( expected = StringIndexOutOfBoundsException.class)
	public void testCheckPasswordDigitsException() {
		boolean password = PasswordValidator.checkPasswordDigits( "passw1" );
		System.out.println(password);
//		assertTrue("Password does not enough numbers ", password==false );
	}
	
// Boundary In Test = use digits atleast 2 and length atleast 8
	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		boolean password = PasswordValidator.checkPasswordDigits( "paswd123" );
		System.out.println(password);
		assertTrue("Password does not have enough numbers " , password );
	}
	

// Boundary Out Test = use digits less than 2 and length less than 8
	@Test ( expected = StringIndexOutOfBoundsException.class)
	public void testCheckPasswordDigitsBoundaryOut() {
		boolean password = PasswordValidator.checkPasswordDigits( "passwd1" );
		System.out.println(password);
//		assertTrue("Password does not have enough numbers " , password );
	}
	
	
	
	
	
	
}
